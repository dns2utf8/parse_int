# CHANGELOG

## 0.6.0

* Fix underscore accepted in radix specifier

## 0.5.0

* Handle input "0" with feature = "implicit-octal" enabled

## 0.4.0

* Accept underscores inside a number

## 0.3.0 and before

* Initial release
* Add CI